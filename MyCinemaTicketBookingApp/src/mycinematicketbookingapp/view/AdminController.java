/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.view;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import mycinematicketbookingapp.data.AppData;
import mycinematicketbookingapp.model.Review;
import mycinematicketbookingapp.model.Ticket;

/**
 *
 * @author Jiarui YU
 */
public class AdminController implements Initializable {
    
    @FXML
    private TextArea reportArea;
            
    @Override
    public void initialize(URL location, ResourceBundle resources) {
	// TODO Auto-generated method stub

    }
    
    public String genSpectatorReport() {
        HashMap<String, Integer> filmSpecator = new HashMap<>();
        for (int i = 0; i < AppData.tickets.size(); i++) {
            Ticket ticket = AppData.tickets.get(i);
            String filmName = ticket.getFilm().getFilmName();
            filmSpecator.put(filmName, filmSpecator.getOrDefault(filmName,0) + 1);
        }
        // sort the film by specator
        List<Map.Entry<String, Integer>> filmList = new ArrayList<>(filmSpecator.entrySet());
        Collections.sort(filmList, (Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) -> (o2.getValue()).compareTo(o1.getValue())); 
        
        HashMap<String, Double> filmScores = new HashMap<>();
        HashMap<String, Integer> filmReviews = new HashMap<>();
        for (int i = 0; i < AppData.reviews.size(); i++) {
            Review review = AppData.reviews.get(i);
            String filmName = review.getFilm().getFilmName();
            filmScores.put(filmName, (double) review.getRating() + filmScores.getOrDefault(filmName, 0.0));
            filmReviews.put(filmName, filmReviews.getOrDefault(filmName, 0) + 1);
        }
        
        // print the report
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Integer> map: filmList) {
            if (filmScores.containsKey(map.getKey())) {
                double score = filmScores.get(map.getKey());
                int reviewNum = filmReviews.get(map.getKey());
                String avgScore = reviewNum != 0 ? Double.toString(score / reviewNum) : "-";
                sb.append(map.getKey()).append(":").append(map.getValue()).append(",").append(avgScore).append("\n");
            }
        }
        if (sb.length() < 1) {
            sb.append("no tickets and reviews in this month.");
        }
        return sb.toString();
        
    }
    
    public void showSpectatorReport() {
        String str = genSpectatorReport();
        System.out.println(str);
        reportArea.setText(str);
    }
    
    public String genIncomeReport() {
        HashMap<String, Integer> filmIncomes = new HashMap<>();
        for (int i = 0; i < AppData.tickets.size(); i++) {
            Ticket ticket = AppData.tickets.get(i);
            String filmName = ticket.getFilm().getFilmName();
            filmIncomes.put(filmName, filmIncomes.getOrDefault(filmName,0) + ticket.getPrice());
        }
        // sort the film by specator
        List<Map.Entry<String, Integer>> filmList = new ArrayList<>(filmIncomes.entrySet());
        Collections.sort(filmList, (Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) -> (o2.getValue()).compareTo(o1.getValue())); 
        
        // print the report
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Integer> map: filmList) {
            sb.append(map.getKey()).append(":").append(map.getValue()).append("\n");
        }
        if (sb.length() < 1) {
            sb.append("no tickets in this month.");
        }
        
        return sb.toString();
    }
    
    public void showIncomeReport() {
        String str = genIncomeReport();
        System.out.println(str);
        reportArea.setText(str);
    }
}
