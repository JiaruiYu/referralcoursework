/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.view;

/**
 *
 * @author 
 */

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.sun.javafx.robot.impl.FXRobotHelper;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mycinematicketbookingapp.data.AppData;

@SuppressWarnings("restriction")
public class LoginController implements Initializable {
	
	@FXML
	private Button SignBtn;
	
	@FXML
	private Button CancelBtn;
	
	@FXML
	private RadioButton RadioBtnUser;
	
	@FXML 
	private RadioButton RadioBtnAdmin;
	
	@FXML
	private TextField NameInput;
	
	@FXML
	private Label NameLabel;
	
	@FXML
	private Label TypeLabel;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		RadioBtnUser.setSelected(true);
		
	}
	
	@FXML
	private void SelectUser() {
		RadioBtnUser.setSelected(true);
		RadioBtnAdmin.setSelected(false);
	}
	
	@FXML
	private void SelectAdmin() {
		RadioBtnUser.setSelected(false);
		RadioBtnAdmin.setSelected(true);
	}
	
	@FXML 
	private void SignIn() throws IOException {
		String userName = NameInput.getText();
		boolean isUser = RadioBtnUser.isSelected();
		boolean isAdmin = RadioBtnAdmin.isSelected();
		
		if (userName.toLowerCase().equals("admin") && isAdmin) {
			// login as admin
			System.out.println("login as admin");
                        AppData.loginUser = userName;
			ObservableList<Stage> stage = FXRobotHelper.getStages(); 
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("Admin.fxml"))); 
			stage.get(0).setScene(scene);
			
		} else if (!userName.toLowerCase().equals("admin") && isUser) {
			// login as user
			System.out.println("login as user");
                        AppData.loginUser = userName;
			ObservableList<Stage> stage = FXRobotHelper.getStages(); 
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("User.fxml"))); 
			stage.get(0).setScene(scene);
                        
		} else {
			System.out.println("wrong name and type");
		}
	}
	
	
	@FXML
	private void Cancel() {
		NameInput.clear();
		RadioBtnUser.setSelected(true);
		RadioBtnAdmin.setSelected(false);
	}
	

}

