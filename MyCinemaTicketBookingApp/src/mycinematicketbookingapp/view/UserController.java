/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.view;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import mycinematicketbookingapp.data.AppData;
import mycinematicketbookingapp.model.Film;
import mycinematicketbookingapp.model.Review;
import mycinematicketbookingapp.model.Screen;
import mycinematicketbookingapp.model.Seat;
import mycinematicketbookingapp.model.ShowTime;
import mycinematicketbookingapp.model.Ticket;
import mycinematicketbookingapp.model.User;

/**
 *
 * @author Jiarui YU
 */
public class UserController implements Initializable {	
    
    @FXML
    private ChoiceBox filmNames;
    
    @FXML
    private DatePicker datePicker;
    
    @FXML
    private ChoiceBox showTimes;
    
    @FXML
    private ChoiceBox screens;
    
    @FXML
    private ChoiceBox seats;
    
    @FXML
    private TextField priceText;
    
    @FXML
    private ListView userTicketsList;
    
    @FXML
    private TextArea commentText;
    
    @FXML
    private TextField ratingText;
    
    @FXML
    private Button ratingBtn;
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub
        ObservableList<String> filmsForChoice =  FXCollections.observableArrayList();
        for (Film film: AppData.films) {
            filmsForChoice.add(film.getFilmName());
        }
        filmNames.setItems(filmsForChoice);
        
        ObservableList<String> showTimesForChoice = FXCollections.observableArrayList(
            ShowTime.AFTERNOON.toString(), ShowTime.EVENING.toString(), 
            ShowTime.NIGHT1.toString(), ShowTime.NIGHT2.toString());
        showTimes.setItems(showTimesForChoice);
        
        ObservableList<String> screensForChoice = FXCollections.observableArrayList();
        for (Screen screen: AppData.screens) {
            screensForChoice.add(Integer.toString(screen.getScreenId()));
        }
        screens.setItems(screensForChoice);
        
        ObservableList<String> seatsForChoice = FXCollections.observableArrayList();
        for (Seat seat: AppData.screens[0].getSeats()) {
            seatsForChoice.add(seat.toString());
        }
        seats.setItems(seatsForChoice);
        
        seats.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            String choicedSeat = (String) observable.getValue();
            if (choicedSeat.endsWith("VIP")) {
                priceText.setText("50");
            } else {
                priceText.setText("30");
            }
        }); 
        
        datePicker.setValue(LocalDate.now());
        
        ObservableList<String> userTickets = FXCollections.observableArrayList();
        for (Ticket ticket:AppData.tickets) {
            if (ticket.getUser().getUserName().equals(AppData.loginUser)) {
                userTickets.add(ticket.toString());
            }
        }
        userTicketsList.setItems(userTickets);
        
        userTicketsList.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            String ticket = (String) observable.getValue();
            if (ticket != null) {
                String filmName = ticket.split(";")[1].split(":")[1].trim();
                String userName = ticket.split(";")[0].split(":")[1].trim();
                for (Review review: AppData.reviews) {
                    if (userName.equals(review.getUser().getUserName()) 
                            && filmName.equals(review.getFilm().getFilmName())) {
                        commentText.setText(review.getComment());
                        ratingText.setText(Integer.toString(review.getRating()));
                        break;
                    } else {
                        commentText.setText(null);
                        ratingText.setText(null);
                    }
                }
            }
        }); 
    }
    
    public void bookTicket() {
        if (!(filmNames.getSelectionModel().getSelectedItem() == null
            || showTimes.getSelectionModel().getSelectedItem() == null 
            || datePicker.getValue() == null 
            || screens.getSelectionModel().getSelectedItem() == null 
            || seats.getSelectionModel().getSelectedItem() == null 
            || priceText.getText() == null 
            || priceText.getText().length() < 1)) {
            
            String choicedFilmName = filmNames.getSelectionModel().getSelectedItem().toString();
            String choicedShowTime = showTimes.getSelectionModel().getSelectedItem().toString();
            String choicedScreen = screens.getSelectionModel().getSelectedItem().toString();
            String choicedSeat = seats.getSelectionModel().getSelectedItem().toString();
            DateTimeFormatter timeFormater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String dateTime = timeFormater.format(datePicker.getValue());
            int price = Integer.parseInt(priceText.getText());
            User user = new User(AppData.loginUser);
            Film film = new Film(choicedFilmName);
            
            ShowTime showTime;
            switch (choicedShowTime) {
                case "AFTERNOON":
                    showTime = ShowTime.AFTERNOON;
                    break;
                case "EVENING":
                    showTime = ShowTime.EVENING;
                    break;
                case "NIGHT1":
                    showTime = ShowTime.NIGHT1;
                    break;
                default:
                    showTime = ShowTime.NIGHT2;
                    break;
            }
            int seatid = Integer.parseInt(choicedSeat.split(":")[0]);
            Ticket ticket = new Ticket(user, film, AppData.screens[Integer.parseInt(choicedScreen)], 
                    AppData.screens[Integer.parseInt(choicedScreen)].getSeats()[seatid], dateTime, showTime, price);
            System.out.println(ticket);
            
            ObservableList<String> userTickets = FXCollections.observableArrayList();
            for (Ticket t: AppData.tickets) {
                if (t.getUser().getUserName().equals(AppData.loginUser)) {
                    userTickets.add(t.toString());
                }
            }
            userTickets.add(0, ticket.toString());
            AppData.tickets.add(ticket);
            userTicketsList.setItems(null);
            userTicketsList.setItems(userTickets);
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Warning");
            alert.setHeaderText(null);
            alert.setContentText("Congratulations on the success of the booking");
            alert.showAndWait();
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText(null);
            alert.setContentText("Please fill in the complete information");
            alert.showAndWait();
        }
    }    
    
    public void ratingFilm() {
        if (userTicketsList.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText(null);
            alert.setContentText("Please select a movie from below");
            alert.showAndWait();
        } else {
            if (commentText.getText() == null || ratingText.getText() == null) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setHeaderText(null);
                alert.setContentText("Please input comment and rating");
                alert.showAndWait();
            } else {
                String ticket = userTicketsList.getSelectionModel().getSelectedItem().toString();
                String filmName = ticket.split(";")[1].trim().split(":")[1].trim();
                Film film = new Film(filmName);
                User user = new User(AppData.loginUser);
                String comment = commentText.getText();
                int rating = Integer.parseInt(ratingText.getText());
                boolean isRating = false;
                for (Review review: AppData.reviews) {
                    if (AppData.loginUser.equals(review.getUser().getUserName()) 
                        && filmName.equals(review.getFilm().getFilmName())) {
                        review.setComment(comment);
                        review.setRating(rating);
                        isRating = true;
                        break;
                    }
                }
                if (!isRating) {
                    Review review = new Review(film, user, rating, comment);
                    AppData.reviews.add(review);
                }
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Warning");
                alert.setHeaderText(null);
                alert.setContentText("Sucess!");
                alert.showAndWait();
            }
        }
    }

}
