/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mycinematicketbookingapp.data.AppData;

/**
 *
 * @author Jiarui YU
 */
public class MyCinemaTicketBookingApp extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        AppData.initAppData();
        Parent root = FXMLLoader.load(getClass().getResource("view/Login.fxml"));
        
        Scene scene = new Scene(root);
        stage.setTitle("Ciname Ticket Booking App");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
