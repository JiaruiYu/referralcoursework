/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.data;

import java.util.ArrayList;
import mycinematicketbookingapp.model.Film;
import mycinematicketbookingapp.model.Review;
import mycinematicketbookingapp.model.Screen;
import mycinematicketbookingapp.model.Seat;
import static mycinematicketbookingapp.model.SeatType.NORMAL;
import static mycinematicketbookingapp.model.SeatType.VIP;
import mycinematicketbookingapp.model.ShowTime;
import mycinematicketbookingapp.model.Ticket;
import mycinematicketbookingapp.model.User;

/**
 *
 * @author Jiarui YU
 */
public class AppData {
    public static final int SCREEN_NUM = 5;
    public static Screen[] screens = new Screen[SCREEN_NUM];
    public static final int NORMAL_SEAT_NUM = 40;
    public static final int VIP_SEAT_NUM = 10;
    public static final int FILM_NUM = 7;
    public static final String[] FILM_NAMES = {"Ant-Man", "Minions", "Jurassic World", "Inside Out", "The Godfather", 
        "The Shawshank Redemption", "Casablanca"};
    public static Film[] films = new Film[FILM_NUM];
    
    public static ArrayList<Ticket> tickets;
    public static ArrayList<Review> reviews;
    
    public static String loginUser;
    
    public static void initAppData () {
        // add films
        for (int i = 0; i < FILM_NUM; i++) {
            Film film = new Film(FILM_NAMES[i]);
            films[i] = film;
        }
        
        // add screens
        for (int i = 0; i < SCREEN_NUM; i++) {
            Seat[] seats = new Seat[NORMAL_SEAT_NUM + VIP_SEAT_NUM];
            for (int j = 0; j < NORMAL_SEAT_NUM; j++) {
                seats[j] = new Seat(j + 1, NORMAL);
            }
            for (int j = 0; j < VIP_SEAT_NUM; j++) {
                seats[NORMAL_SEAT_NUM + j] = new Seat(NORMAL_SEAT_NUM + j + 1, VIP);
            }
            screens[i] = new Screen(i + 1, seats);
        }
        
        // store the tickets and reviews
        tickets = new ArrayList<>();
        String[] user_names = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
        ShowTime[] showTimes = {ShowTime.AFTERNOON, ShowTime.EVENING, ShowTime.NIGHT1, 
            ShowTime.NIGHT2, ShowTime.AFTERNOON, ShowTime.EVENING, ShowTime.NIGHT1, 
            ShowTime.NIGHT2, ShowTime.AFTERNOON, ShowTime.EVENING};
        int[] prices = {30, 50, 30, 30, 30, 50, 30, 50, 30, 30, 30, 30};
        String[] dates = {"2018-05-15", "2018-05-16", "2018-05-17", "2018-05-18", 
        "2018-05-19", "2018-05-20", "20188-05-21", "2018-05-22", "2018-05-23", "2018-05-24", "2018-05-25"};
        for (int i = 0; i < 10; i++) {
            User user = new User(user_names[i]);
            Film film = new Film(films[i % films.length].getFilmName());
            ShowTime showTime = showTimes[i];
            int price = prices[i];
            String date = dates[i];
            Screen screen = screens[i % 5];
            Seat seat = screen.getSeats()[i];
            if (price == 50) {
                seat.setSeatId(i +  40);
                seat.setSeatType(VIP);
            }
            Ticket ticket = new Ticket(user, film, screen, seat, date, showTime, price);
            tickets.add(ticket);
        }

        reviews = new ArrayList<>();
        int[] ratings = {3, 1, 4, 5, 2};
        String[] comments = {"just so so", "bad film", "good", "very good", "regret watch it"};
        for (int i = 0; i < 5; i++) {
            Film film = new Film(films[i].getFilmName());
            User user = new User(user_names[i]);
            int rate = ratings[i];
            String comment = comments[i];
            Review review = new Review(film, user, rate, comment);
            reviews.add(review);
        }
    }
    
}
