/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.model;

/**
 *
 * @author Jiarui YU
 */
public class Ticket {
    private User user;
    private Film film;
    private Screen screen;
    private Seat seat;
    private ShowTime showTime;
    private String date;
    private int price;
    
    public Ticket (User user, Film film, Screen screen, Seat seat, 
            String date, ShowTime showTime, int price) {
        this.user = user;
        this.film = film;
        this.screen = screen;
        this.seat = seat;
        this.date = date;
        this.showTime = showTime;
        this.price = price;
    }
    
    public void setUser (User user) {
        this.user = user;
    }
    
    public User getUser () {
        return this.user;
    }
    
    public void setFilm (Film film) {
        this.film = film;
    }
    
    public Film getFilm () {
        return this.film;
    }
    
    public void setScreen(Screen screen) {
        this.screen = screen;
    }
    
    public Screen getScreen() {
        return this.screen;
    }
    
    public void setSeat(Seat seat) {
        this.seat = seat;
    }
    
    public Seat getSeat() {
        return this.seat;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public String getDate() {
        return this.date;
    }
    
    public void setShowTime(ShowTime showTime) {
        this.showTime = showTime;
    }
    
    public ShowTime getShowTime() {
        return this.showTime;
    }
    
    public void setPrice (int price) {
        this.price = price;
    }
    
    public int getPrice () {
        return this.price;
    }
    
    @Override
    public String toString() {
        return "User: " + this.user.getUserName() +  "; " 
                + "Film: " + this.film.getFilmName() + "; "
                + "Screen: " + this.screen.getScreenId() + "; " 
                + "Seat: " + this.seat.getSeatId() + " " + this.seat.getSeatType().toString() + "; "
                + "Date: " + this.date + "; "
                + "ShowTime: " + this.showTime.toString() + "; " 
                + "Price: " + this.price;
    }
} 
