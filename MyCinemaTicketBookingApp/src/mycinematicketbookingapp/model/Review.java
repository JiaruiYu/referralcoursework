/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.model;

/**
 *
 * @author Jiarui YU
 */
public class Review {
    private static int ID_COUNTER = 1;
    private int id;
    private int rating;
    private String comment;
    private Film film;
    private User user;
    
    public Review(Film film, User user, int rating, String comment) {
        this.id = ID_COUNTER;
        this.film = film;
        this.user = user;
        this.rating = rating;
        this.comment = comment;
        ID_COUNTER += 1;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setRating(int rating) {
        this.rating = rating;
    }
    
    public int getRating() {
        return this.rating;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public String getComment() {
        return this.comment;
    }
    
    public void SetFilm(Film film) {
        this.film = film;
    }
    
    public Film getFilm() {
        return this.film;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return this.user;
    }
    
    @Override
    public String toString() {
        return "User: " + this.user.getUserName() 
                + ", Film: " + this.film.getFilmName()
                + ", Rating: " + this.rating 
                + ",Comments: " + this.comment;
    }
}

