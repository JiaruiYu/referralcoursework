/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.model;

/**
 *
 * @author Jiarui YU
 */
public class Seat {
    private int seatId;
    private SeatType seatType;
	
    public Seat(int seatId, SeatType st) {
    	this.seatId = seatId;
	this.seatType = st;
    }

    public SeatType getSeatType() {
	return seatType;
    }

    public void setSeatType(SeatType searType) {
	this.seatType = searType;
    }

    public int getSeatId() {
	return seatId;
    }

    public void setSeatId(int seatId) {
	this.seatId = seatId;
    }

    @Override
    public String toString () {
        return this.seatId + ": " + this.seatType.toString();
    }
}

