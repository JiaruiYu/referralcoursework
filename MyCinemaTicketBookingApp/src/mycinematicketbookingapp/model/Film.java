/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.model;

/**
 *
 * @author Jiarui YU
 */
public class Film {
    private String filmName;
    
    public Film (String name) {
        this.filmName = name;
    }
    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

}
