/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.model;

/**
 *
 * @author  Jiarui YU
 */
public class Screen {
    private int screenId;
    private Seat[] seats;
	
    public Screen (int screenId, Seat[] seats) {
    	this.screenId = screenId;
	this.seats = seats;
    }

    public Seat[] getSeats() {
	return seats;
    }

    public void setSeats(Seat[] seats) {
	this.seats = seats;
    }

    public int getScreenId() {
	return screenId;
    }

    public void setScreenId(int screenId) {
        this.screenId = screenId;
    } 
	
}
