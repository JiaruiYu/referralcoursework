/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp.model;

/**
 *
 * @author Jiarui YU
 */
public enum ShowTime {
    AFTERNOON, EVENING, NIGHT1, NIGHT2
}
