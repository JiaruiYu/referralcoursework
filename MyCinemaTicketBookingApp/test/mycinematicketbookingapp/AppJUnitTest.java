/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycinematicketbookingapp;

import mycinematicketbookingapp.data.AppData;
import mycinematicketbookingapp.view.AdminController;
import mycinematicketbookingapp.view.UserController;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author liupenghe
 */
public class AppJUnitTest {
    
    public static AdminController adminController = new AdminController();
    
    public AppJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        AppData.initAppData();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testAdminSpectatorReport() {
        String spectatorReport = adminController.genSpectatorReport();
        assertEquals(spectatorReport, "Minions:2,1.0\n" +
                "Ant-Man:2,3.0\n" +
                "Jurassic World:2,4.0\n" +
                "The Godfather:1,2.0\n" +
                "Inside Out:1,5.0\n");
    }
    
    @Test
    public void testAdminIncomeReport() {
        String incomeReport = adminController.genIncomeReport();
        assertEquals(incomeReport, "Minions:80\n" +
            "Ant-Man:80\n" +
            "Jurassic World:60\n" +
            "The Shawshank Redemption:50\n" +
            "The Godfather:30\n" +
            "Casablanca:30\n" +
            "Inside Out:30\n");
    }
   
}
